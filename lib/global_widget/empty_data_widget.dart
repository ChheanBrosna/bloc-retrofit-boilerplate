import 'package:flutter/material.dart';

class EmptyDataWidget extends StatelessWidget {
  const EmptyDataWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'NO DATA',
        style: Theme.of(context).textTheme.titleMedium,
      ),
    );
  }
}
