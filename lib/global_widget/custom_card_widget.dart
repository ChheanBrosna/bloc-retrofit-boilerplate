import 'package:bloc_retrofit_boilerplate/singleton/singleton_index.dart';
import 'package:flutter/material.dart';

class CustomCardWidget extends StatelessWidget {
  const CustomCardWidget({
    Key? key,
    this.padding,
    this.margin,
    this.color,
    this.borderRadius,
    this.child,
  }) : super(key: key);

  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final Color? color;
  final BorderRadiusGeometry? borderRadius;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      margin: margin,
      decoration: BoxDecoration(
        color: color ?? Colors.white,
        borderRadius: borderRadius ??
            BorderRadius.circular(Metrics.instance.defaultRadius),
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 1,
          ),
        ],
      ),
      child: child,
    );
  }
}
