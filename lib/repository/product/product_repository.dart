import 'package:bloc_retrofit_boilerplate/constant/constant_index.dart';
import 'package:bloc_retrofit_boilerplate/model/model_index.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'product_repository.g.dart';

@RestApi(baseUrl: UrlConstant.baseUrl)
abstract class ProductRepository {
  factory ProductRepository(Dio dio, {String baseUrl}) = _ProductRepository;

  @GET(EndPointConstant.getProduct)
  Future<ProductResponseModel> getProduct();
}
