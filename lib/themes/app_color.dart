import 'package:flutter/material.dart';

class AppColors {
  // Native color
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF000000);
  static const primary = Color(0xff20a901);
  static const lightPrimary = Color(0xFFE2FADE);
  static const secondary = Color(0xFF7983D9);
  static const darkBlue = Color(0xff0F253F);
  static const red = Color(0xFFEB1D1D);

  // App background color
  static const bgColor = Color(0xffEFEFF4);

  // Text field color
  static const textFieldBorderColor = Color(0xFFD8D8D8);
  static const textFieldBgColor = Color(0xFFFFFFFF);

  // Text color
  static const primaryTextColor = Color(0xFF353E45);
}
