import 'package:flutter_device_type/flutter_device_type.dart';

abstract class MetricsSingleton {
  // Regular Size
  double get extraHuge;

  double get huge;

  double get large;

  double get medium;

  double get small;

  double get tiny;

  // Display text size
  double get displayLarge;

  double get displayMedium;

  double get displaySmall;

  // Headline text size
  double get headlineLarge;

  double get headlineMedium;

  double get headlineSmall;

  // Title text size
  double get titleLarge;

  double get titleMedium;

  double get titleSmall;

  // Sub-title text size
  double get subTitle1;

  double get subTitle2;

  // Body text size
  double get bodyLarge;

  double get bodyMedium;

  double get bodySmall;

  // Text

  // Appbar text size
  double get appBarSize;

  // TextField size
  double get textFieldVerticalPadding;

  double get textFieldHorizontalPadding;

  double get textFieldBorderRadius;

  // Button size
  double get buttonHeight;

  double get buttonRadius;

  // Padding size
  double get defaultPadding;

  // Radius size
  double get defaultRadius;
}

/// Handle mobile size
class _MobileMetrics extends MetricsSingleton {
  _MobileMetrics();

  // Regular Size
  @override
  double get extraHuge => 64.0;

  @override
  double get huge => 32.0;

  @override
  double get large => 17.0;

  @override
  double get medium => 12.0;

  @override
  double get small => 6.0;

  @override
  double get tiny => 3.0;

  // Body text size
  @override
  double get bodyLarge => 19.0;

  @override
  double get bodyMedium => 16.0;

  @override
  double get bodySmall => 14.0;

  // Display text size
  @override
  double get displayLarge => 92.0;

  @override
  double get displayMedium => 57.0;

  @override
  double get displaySmall => 46.0;

  // Headline text size
  @override
  double get headlineLarge => 32.0;

  @override
  double get headlineMedium => 23.0;

  @override
  double get headlineSmall => 19.0;

  // Sub-title text size
  @override
  double get subTitle1 => 19.0;

  @override
  double get subTitle2 => 17.0;

  // Title text size
  @override
  double get titleLarge => 24.0;

  @override
  double get titleMedium => 21.0;

  @override
  double get titleSmall => 19.0;

  @override
  double get appBarSize => 17.0;

  @override
  double get textFieldHorizontalPadding => 30.0;

  @override
  double get textFieldVerticalPadding => 18.0;

  @override
  double get textFieldBorderRadius => 50.0;

  // Padding size
  @override
  double get defaultPadding => 16.0;

  // Button size
  @override
  double get buttonHeight => 50.0;

  @override
  double get buttonRadius => 10.0;

  @override
  double get defaultRadius => 8.0;
}

/// Handle tablet size
class _TabletMetrics extends MetricsSingleton {
  _TabletMetrics();

  // Regular Size
  @override
  double get extraHuge => 70.0;

  @override
  double get huge => 38.0;

  @override
  double get large => 23.0;

  @override
  double get medium => 18.0;

  @override
  double get small => 12.0;

  @override
  double get tiny => 9.0;

  // Display text size
  @override
  double get displayLarge => 100.0;

  @override
  double get displayMedium => 65.0;

  @override
  double get displaySmall => 54.0;

  // Headline text size
  @override
  double get headlineLarge => 38.0;

  @override
  double get headlineMedium => 29.0;

  @override
  double get headlineSmall => 25.0;

  // Body text size
  @override
  double get bodyLarge => 23.0;

  @override
  double get bodyMedium => 20.0;

  @override
  double get bodySmall => 18.0;

  // Sub-title text size
  @override
  double get subTitle1 => 23.0;

  @override
  double get subTitle2 => 21.0;

  // Title text size
  @override
  double get titleLarge => 29.0;

  @override
  double get titleMedium => 26.0;

  @override
  double get titleSmall => 24.0;

  // Appbar size
  @override
  double get appBarSize => 21.0;

  @override
  double get textFieldHorizontalPadding => 23.0;

  @override
  double get textFieldVerticalPadding => 35.0;

  @override
  double get textFieldBorderRadius => 55.0;

  // Padding size
  @override
  double get defaultPadding => 20.0;

  // Button size
  @override
  double get buttonHeight => 54.0;

  @override
  double get buttonRadius => 14.0;

  @override
  double get defaultRadius => 12.0;
}

class Metrics {
  static MetricsSingleton instance =
      Device.get().isPhone ? _MobileMetrics() : _TabletMetrics();
}
