import 'dart:developer';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncher {
  Future<void> launchURL({required String url}) async {
    final Uri uri = Uri.parse(url);
    try {
      !await launchUrl(uri);
    } on Exception catch (e) {
      log(e.toString());
      rethrow;
    }
  }
}
