import 'package:bloc_retrofit_boilerplate/global_widget/dialog/dialog_index.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class UiErrorUtils {
  /// opens snackbar
  static void openSnackBar(BuildContext context, String message) async {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  /// subscribes to stream that triggers open dialog
  static void subscribeToDialogStream({
    required BuildContext context,
    required PublishSubject<String> stream,
    required String title,
    final Function()? onPressed,
  }) {
    stream.listen((String message) {
      showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ErrorDialogWidget(
            title: title,
            content: message,
            onPressed: onPressed,
          );
        },
      );
    });
  }
}
