import 'dart:developer';
import 'package:bloc_retrofit_boilerplate/constant/constant_index.dart';
import 'package:bloc_retrofit_boilerplate/model/model_index.dart';
import 'package:bloc_retrofit_boilerplate/storage/storage_index.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Handle DIO Interceptor
class DIOInterceptor extends Interceptor {
  DIOInterceptor();

  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final newOptions = await _setHeaders(options);
    log('===== API CALL =====');
    log('${newOptions.method}: ${newOptions.uri.toString()}');
    log('===== PAYLOADS =====');
    log(newOptions.data.toString());
    return super.onRequest(newOptions, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log('===== RESPONSE ===== $response');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    log('===== ERROR ===== $err : ${err.response}');
    if (err.type == DioErrorType.response) {
      switch (err.response?.statusCode) {
        case 401:
          err.error = const Unauthorized().message;
          break;
        case 403:
          err.error = const Forbidden().message;
          break;
        case 404:
          err.error = const NotFound().message;
          break;
        case 500:
          err.error = const InternalServerError().message;
          break;
      }
    }

    if (err.type == DioErrorType.cancel) err.error = const UserCancelled();

    if (err.type == DioErrorType.sendTimeout ||
        err.type == DioErrorType.receiveTimeout) {
      err.error = const ServerTimeOut().message;
    }

    if (err.type == DioErrorType.connectTimeout) {
      err.error = const NoConnection().message;
    }

    if (err.type == DioErrorType.other) {
      err.error = const Other().message;
    }

    super.onError(err, handler);
  }

  /// Set headers
  Future<RequestOptions> _setHeaders(RequestOptions options) async {
    final authorization = options.headers['Authorization'];
    final accessToken = await _getToken();
    log('===== TOKEN ===== ${accessToken.toString()}');
    if (authorization == null) {
      options.headers['Authorization'] = accessToken;
    }
    return options;
  }

  /// Get token from local storage
  Future<String?> _getToken() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    final AppStorage appStorage =
        AppStorage(sharedPreferences: sharedPreferences);

    return 'Bearer ${appStorage.readString(key: StorageKeyConstant.token.toString())}';
  }
}
