import 'package:bloc_retrofit_boilerplate/global_widget/global_widget_index.dart';
import 'package:bloc_retrofit_boilerplate/model/product/product_model.dart';
import 'package:bloc_retrofit_boilerplate/singleton/singleton_index.dart';
import 'package:flutter/material.dart';

class ProductItemWidget extends StatelessWidget {
  const ProductItemWidget({
    Key? key,
    required this.productModel,
    required this.onTap,
  }) : super(key: key);

  final ProductModel productModel;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: CustomCardWidget(
        padding: EdgeInsets.all(Metrics.instance.defaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  productModel.title!,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                Text(
                  '\$ ${productModel.price!.toString()}',
                  style: Theme.of(context)
                      .textTheme
                      .bodySmall!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ],
            ),
            SizedBox(height: Metrics.instance.medium),
            Text(
              productModel.description!,
              maxLines: 1,
              style:
                  Theme.of(context).textTheme.bodySmall!.copyWith(height: 1.5),
            ),
          ],
        ),
      ),
    );
  }
}
