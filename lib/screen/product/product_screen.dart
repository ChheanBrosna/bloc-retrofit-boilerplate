import 'dart:developer';

import 'package:bloc_retrofit_boilerplate/bloc/bloc_index.dart';
import 'package:bloc_retrofit_boilerplate/global_widget/global_widget_index.dart';
import 'package:bloc_retrofit_boilerplate/screen/product/widgets/product_widgets_index.dart';
import 'package:bloc_retrofit_boilerplate/singleton/singleton_index.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ProductScreenWrapper extends StatelessWidget {
  const ProductScreenWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProductBloc>(
      create: (context) => ProductBloc(),
      child: ProductScreen(),
    );
  }
}

class ProductScreen extends HookWidget {
  ProductScreen({Key? key}) : super(key: key);

  final EasyRefreshController _refreshController = EasyRefreshController(
      controlFinishRefresh: true, controlFinishLoad: true);

  @override
  Widget build(BuildContext context) {
    useEffect(() {
      log('===== ProductScreen OPEN =====');
      // Fetch Product
      context.read<ProductBloc>().add(const ProductEvent.fetchProduct());
      return () {
        _refreshController.dispose();
        log('===== ProductScreen DISPOSE =====');
      };
    }, const []);

    Future<void> productHandleRefresh() async {
      context.read<ProductBloc>().add(const ProductEvent.fetchProduct());
    }

    Future<void> productHandleLoad() async {
      context.read<ProductBloc>().add(const ProductEvent.fetchProduct());
    }

    void productItemHandleTap() {
      showDialog(
        context: context,
        builder: (context) => const ErrorDialogWidget(),
      );
    }

    return Scaffold(
      appBar: AppBar(title: const Text('PRODUCTS')),
      body: BlocBuilder<ProductBloc, ProductState>(
        buildWhen: (previous, current) {
          // First Fetch
          if (previous is ProductFetchInProgress &&
              current is ProductFetchSucceed) return true;

          // First Fetch Failed include connection too
          if (previous is ProductFetchInProgress &&
              current is ProductFetchFailed) return true;

          // Later Fetch
          // ProductFetchSucceed & ProductFetchInProgress to clear screen
          // ProductFetchSucceed & ProductFetchFailed to show NO DATA
          if (previous is ProductFetchSucceed &&
              (current is ProductFetchInProgress ||
                  current is ProductFetchFailed)) return true;

          return false;
        },
        builder: (context, state) {
          if (state is ProductFetchSucceed) {
            final products = state.productResponseModel.products;
            if (products.isEmpty) return const EmptyDataWidget();
            return EasyRefresh(
              controller: _refreshController,
              header: const MaterialHeader(),
              footer: const ClassicFooter(),
              onRefresh: productHandleRefresh,
              onLoad: productHandleLoad,
              child: ListView.builder(
                padding: EdgeInsets.only(
                  left: Metrics.instance.defaultPadding,
                  right: Metrics.instance.defaultPadding,
                  top: Metrics.instance.defaultPadding,
                ),
                itemCount: products.length,
                itemBuilder: (context, index) => Padding(
                  padding:
                      EdgeInsets.only(bottom: Metrics.instance.defaultPadding),
                  child: ProductItemWidget(
                    productModel: products[index],
                    onTap: productItemHandleTap,
                  ),
                ),
              ),
            );
          }
          if (state is ProductFetchFailed) return const EmptyDataWidget();
          return const SizedBox.shrink();
        },
      ),
    );
  }
}
