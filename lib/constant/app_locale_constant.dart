import 'package:flutter/material.dart';

class AppLocales {
  static const Locale en = Locale('en');
  static const Locale km = Locale('km');
  static const Locale zh = Locale('zh');

  static const path = 'assets/translation';
}
