import 'package:auto_route/auto_route.dart';
import 'package:bloc_retrofit_boilerplate/screen/screen_index.dart';
import 'package:bloc_retrofit_boilerplate/start_up_screen.dart';

/// Handle route management using auto_route
@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: StartUpScreen, initial: true),
    AutoRoute(name: 'ProductRouter', page: ProductScreenWrapper),
  ],
)
class $AppRouter {}
