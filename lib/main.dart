import 'package:auto_route/auto_route.dart';
import 'package:bloc_retrofit_boilerplate/bloc/bloc_index.dart';
import 'package:bloc_retrofit_boilerplate/constant/constant_index.dart';
import 'package:bloc_retrofit_boilerplate/themes/themes_index.dart';
import 'package:bloc_retrofit_boilerplate/util/util_index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:get_it/get_it.dart';
import 'package:bloc_retrofit_boilerplate/app_router/app_router.gr.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rxdart/subjects.dart';

final PublishSubject<String> globalUiErrorSubject = PublishSubject<String>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  GetIt.instance.registerSingleton<AppRouter>(AppRouter());

  runApp(
    MultiBlocProvider(
      // Note : All bloc inside this provider WILL NOT DISPOSE untill app KILL
      providers: [
        BlocProvider<ProfileBloc>(create: (context) => ProfileBloc()),
      ],
      child: EasyLocalization(
        supportedLocales: const [
          AppLocales.en,
          AppLocales.km,
        ],
        startLocale: const Locale('en'),
        path: AppLocales.path,
        fallbackLocale: const Locale('en'),
        useOnlyLangCode: true,
        child: const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: MyApp(),
        ),
      ),
    ),
  );
}

class MyApp extends HookWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final router = GetIt.instance<AppRouter>();

    return MaterialApp.router(
      debugShowCheckedModeBanner: false,

      title: AppConstant.appName,
      theme: AppTheme.define(),

      // route config
      routeInformationParser: router.defaultRouteParser(),
      routerDelegate: AutoRouterDelegate(router),

      // locale config
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,

      // dismiss keyboard when click on screen
      builder: (context, child) {
        return GestureDetector(
          onTap: () => KeyboardHidden.hideKeyboard(context),
          child: child,
        );
      },
    );
  }
}
