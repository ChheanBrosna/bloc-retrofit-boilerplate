/// To add more functions member in String class
extension StringExt on String {
  String get removeUnderScore {
    return replaceAll('_', ' ');
  }
}
