import 'package:flutter/material.dart';

/// To add more property in BuildContext class
extension ContextExtension on BuildContext {
  double get screenHeight => MediaQuery.of(this).size.height;
  double get screenWidth => MediaQuery.of(this).size.width;
  double get appBarHeight => AppBar().preferredSize.height;
  double get paddingTop => MediaQuery.of(this).padding.top;
  double get paddingButton => MediaQuery.of(this).padding.bottom;
  double get paddingLeft => MediaQuery.of(this).padding.left;
  double get paddingRight => MediaQuery.of(this).padding.right;
}
