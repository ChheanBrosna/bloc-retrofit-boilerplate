import 'package:bloc_retrofit_boilerplate/singleton/singleton_index.dart';
import 'package:bloc_retrofit_boilerplate/themes/app_color.dart';
import 'package:bloc_retrofit_boilerplate/util/util_index.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

mixin DIOMixin {
  final connectivity = Connectivity();

  /// Get DIO which have already added interceptor
  Future<Dio?> getDio() async {
    final connectivityResult = await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      Fluttertoast.showToast(
        msg: tr('message.unableToConnectToTheInternet'),
        backgroundColor: AppColors.primary,
        textColor: Colors.white,
        toastLength: Toast.LENGTH_LONG,
        fontSize: Metrics.instance.bodyMedium,
      );
      return null;
    } else {
      final Dio dio = Dio();
      dio.interceptors.add(DIOInterceptor());
      return dio;
    }
  }
}
