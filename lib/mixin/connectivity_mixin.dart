import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

mixin ConnectivityMixin {
  final connectivity = Connectivity();

  /// To check this device's network connectivity
  Future<bool> isInConnection() async {
    var connectivityResult = await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      Fluttertoast.showToast(
        msg: tr('message.unableToConnectToTheInternet'),
        backgroundColor: Colors.grey,
        textColor: Colors.white,
      );
      return false;
    }
    return true;
  }
}
