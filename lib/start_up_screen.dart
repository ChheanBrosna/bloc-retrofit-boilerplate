import 'package:bloc_retrofit_boilerplate/app_router/app_router.gr.dart';
import 'package:bloc_retrofit_boilerplate/main.dart';
import 'package:bloc_retrofit_boilerplate/singleton/singleton_index.dart';
import 'package:bloc_retrofit_boilerplate/util/ui_error_util.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StartUpScreen extends HookWidget {
  const StartUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    useEffect(() {
      UiErrorUtils.subscribeToDialogStream(
        context: context,
        stream: globalUiErrorSubject,
        title: 'Error',
      );
      return () {};
    }, const []);

    return Scaffold(
      appBar: AppBar(
        title: const Text('START-UP'),
      ),
      body: Padding(
        padding: EdgeInsets.all(Metrics.instance.defaultPadding),
        child: Center(
          child: ElevatedButton(
            onPressed: () {
              context.navigateTo(const ProductRouter());
            },
            child: const Text('PRODUCT'),
          ),
        ),
      ),
    );
  }
}
