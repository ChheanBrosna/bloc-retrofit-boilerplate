import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_state.dart';
part 'profile_event.dart';
part 'profile_bloc.freezed.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(const ProfileState.profileNotFetch()) {
    on<_FetchProfile>(_onFetchProfile);
  }
  void _onFetchProfile(_FetchProfile event, Emitter<ProfileState> emit) {}
}
