// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'profile_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() profileNotFetch,
    required TResult Function() profileFetchInProgress,
    required TResult Function() profileFetchSucceed,
    required TResult Function() profileFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? profileNotFetch,
    TResult? Function()? profileFetchInProgress,
    TResult? Function()? profileFetchSucceed,
    TResult? Function()? profileFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? profileNotFetch,
    TResult Function()? profileFetchInProgress,
    TResult Function()? profileFetchSucceed,
    TResult Function()? profileFetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileNotFetch value) profileNotFetch,
    required TResult Function(ProfileFetchInProgress value)
        profileFetchInProgress,
    required TResult Function(ProfileFetchSucceed value) profileFetchSucceed,
    required TResult Function(ProfileFetchFailed value) profileFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileNotFetch value)? profileNotFetch,
    TResult? Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult? Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult? Function(ProfileFetchFailed value)? profileFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileNotFetch value)? profileNotFetch,
    TResult Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult Function(ProfileFetchFailed value)? profileFetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res, ProfileState>;
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res, $Val extends ProfileState>
    implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ProfileNotFetchCopyWith<$Res> {
  factory _$$ProfileNotFetchCopyWith(
          _$ProfileNotFetch value, $Res Function(_$ProfileNotFetch) then) =
      __$$ProfileNotFetchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileNotFetchCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res, _$ProfileNotFetch>
    implements _$$ProfileNotFetchCopyWith<$Res> {
  __$$ProfileNotFetchCopyWithImpl(
      _$ProfileNotFetch _value, $Res Function(_$ProfileNotFetch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileNotFetch implements ProfileNotFetch {
  const _$ProfileNotFetch();

  @override
  String toString() {
    return 'ProfileState.profileNotFetch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileNotFetch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() profileNotFetch,
    required TResult Function() profileFetchInProgress,
    required TResult Function() profileFetchSucceed,
    required TResult Function() profileFetchFailed,
  }) {
    return profileNotFetch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? profileNotFetch,
    TResult? Function()? profileFetchInProgress,
    TResult? Function()? profileFetchSucceed,
    TResult? Function()? profileFetchFailed,
  }) {
    return profileNotFetch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? profileNotFetch,
    TResult Function()? profileFetchInProgress,
    TResult Function()? profileFetchSucceed,
    TResult Function()? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileNotFetch != null) {
      return profileNotFetch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileNotFetch value) profileNotFetch,
    required TResult Function(ProfileFetchInProgress value)
        profileFetchInProgress,
    required TResult Function(ProfileFetchSucceed value) profileFetchSucceed,
    required TResult Function(ProfileFetchFailed value) profileFetchFailed,
  }) {
    return profileNotFetch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileNotFetch value)? profileNotFetch,
    TResult? Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult? Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult? Function(ProfileFetchFailed value)? profileFetchFailed,
  }) {
    return profileNotFetch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileNotFetch value)? profileNotFetch,
    TResult Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult Function(ProfileFetchFailed value)? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileNotFetch != null) {
      return profileNotFetch(this);
    }
    return orElse();
  }
}

abstract class ProfileNotFetch implements ProfileState {
  const factory ProfileNotFetch() = _$ProfileNotFetch;
}

/// @nodoc
abstract class _$$ProfileFetchInProgressCopyWith<$Res> {
  factory _$$ProfileFetchInProgressCopyWith(_$ProfileFetchInProgress value,
          $Res Function(_$ProfileFetchInProgress) then) =
      __$$ProfileFetchInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileFetchInProgressCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res, _$ProfileFetchInProgress>
    implements _$$ProfileFetchInProgressCopyWith<$Res> {
  __$$ProfileFetchInProgressCopyWithImpl(_$ProfileFetchInProgress _value,
      $Res Function(_$ProfileFetchInProgress) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileFetchInProgress implements ProfileFetchInProgress {
  const _$ProfileFetchInProgress();

  @override
  String toString() {
    return 'ProfileState.profileFetchInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileFetchInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() profileNotFetch,
    required TResult Function() profileFetchInProgress,
    required TResult Function() profileFetchSucceed,
    required TResult Function() profileFetchFailed,
  }) {
    return profileFetchInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? profileNotFetch,
    TResult? Function()? profileFetchInProgress,
    TResult? Function()? profileFetchSucceed,
    TResult? Function()? profileFetchFailed,
  }) {
    return profileFetchInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? profileNotFetch,
    TResult Function()? profileFetchInProgress,
    TResult Function()? profileFetchSucceed,
    TResult Function()? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchInProgress != null) {
      return profileFetchInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileNotFetch value) profileNotFetch,
    required TResult Function(ProfileFetchInProgress value)
        profileFetchInProgress,
    required TResult Function(ProfileFetchSucceed value) profileFetchSucceed,
    required TResult Function(ProfileFetchFailed value) profileFetchFailed,
  }) {
    return profileFetchInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileNotFetch value)? profileNotFetch,
    TResult? Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult? Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult? Function(ProfileFetchFailed value)? profileFetchFailed,
  }) {
    return profileFetchInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileNotFetch value)? profileNotFetch,
    TResult Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult Function(ProfileFetchFailed value)? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchInProgress != null) {
      return profileFetchInProgress(this);
    }
    return orElse();
  }
}

abstract class ProfileFetchInProgress implements ProfileState {
  const factory ProfileFetchInProgress() = _$ProfileFetchInProgress;
}

/// @nodoc
abstract class _$$ProfileFetchSucceedCopyWith<$Res> {
  factory _$$ProfileFetchSucceedCopyWith(_$ProfileFetchSucceed value,
          $Res Function(_$ProfileFetchSucceed) then) =
      __$$ProfileFetchSucceedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileFetchSucceedCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res, _$ProfileFetchSucceed>
    implements _$$ProfileFetchSucceedCopyWith<$Res> {
  __$$ProfileFetchSucceedCopyWithImpl(
      _$ProfileFetchSucceed _value, $Res Function(_$ProfileFetchSucceed) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileFetchSucceed implements ProfileFetchSucceed {
  const _$ProfileFetchSucceed();

  @override
  String toString() {
    return 'ProfileState.profileFetchSucceed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileFetchSucceed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() profileNotFetch,
    required TResult Function() profileFetchInProgress,
    required TResult Function() profileFetchSucceed,
    required TResult Function() profileFetchFailed,
  }) {
    return profileFetchSucceed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? profileNotFetch,
    TResult? Function()? profileFetchInProgress,
    TResult? Function()? profileFetchSucceed,
    TResult? Function()? profileFetchFailed,
  }) {
    return profileFetchSucceed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? profileNotFetch,
    TResult Function()? profileFetchInProgress,
    TResult Function()? profileFetchSucceed,
    TResult Function()? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchSucceed != null) {
      return profileFetchSucceed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileNotFetch value) profileNotFetch,
    required TResult Function(ProfileFetchInProgress value)
        profileFetchInProgress,
    required TResult Function(ProfileFetchSucceed value) profileFetchSucceed,
    required TResult Function(ProfileFetchFailed value) profileFetchFailed,
  }) {
    return profileFetchSucceed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileNotFetch value)? profileNotFetch,
    TResult? Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult? Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult? Function(ProfileFetchFailed value)? profileFetchFailed,
  }) {
    return profileFetchSucceed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileNotFetch value)? profileNotFetch,
    TResult Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult Function(ProfileFetchFailed value)? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchSucceed != null) {
      return profileFetchSucceed(this);
    }
    return orElse();
  }
}

abstract class ProfileFetchSucceed implements ProfileState {
  const factory ProfileFetchSucceed() = _$ProfileFetchSucceed;
}

/// @nodoc
abstract class _$$ProfileFetchFailedCopyWith<$Res> {
  factory _$$ProfileFetchFailedCopyWith(_$ProfileFetchFailed value,
          $Res Function(_$ProfileFetchFailed) then) =
      __$$ProfileFetchFailedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileFetchFailedCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res, _$ProfileFetchFailed>
    implements _$$ProfileFetchFailedCopyWith<$Res> {
  __$$ProfileFetchFailedCopyWithImpl(
      _$ProfileFetchFailed _value, $Res Function(_$ProfileFetchFailed) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileFetchFailed implements ProfileFetchFailed {
  const _$ProfileFetchFailed();

  @override
  String toString() {
    return 'ProfileState.profileFetchFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileFetchFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() profileNotFetch,
    required TResult Function() profileFetchInProgress,
    required TResult Function() profileFetchSucceed,
    required TResult Function() profileFetchFailed,
  }) {
    return profileFetchFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? profileNotFetch,
    TResult? Function()? profileFetchInProgress,
    TResult? Function()? profileFetchSucceed,
    TResult? Function()? profileFetchFailed,
  }) {
    return profileFetchFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? profileNotFetch,
    TResult Function()? profileFetchInProgress,
    TResult Function()? profileFetchSucceed,
    TResult Function()? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchFailed != null) {
      return profileFetchFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileNotFetch value) profileNotFetch,
    required TResult Function(ProfileFetchInProgress value)
        profileFetchInProgress,
    required TResult Function(ProfileFetchSucceed value) profileFetchSucceed,
    required TResult Function(ProfileFetchFailed value) profileFetchFailed,
  }) {
    return profileFetchFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileNotFetch value)? profileNotFetch,
    TResult? Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult? Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult? Function(ProfileFetchFailed value)? profileFetchFailed,
  }) {
    return profileFetchFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileNotFetch value)? profileNotFetch,
    TResult Function(ProfileFetchInProgress value)? profileFetchInProgress,
    TResult Function(ProfileFetchSucceed value)? profileFetchSucceed,
    TResult Function(ProfileFetchFailed value)? profileFetchFailed,
    required TResult orElse(),
  }) {
    if (profileFetchFailed != null) {
      return profileFetchFailed(this);
    }
    return orElse();
  }
}

abstract class ProfileFetchFailed implements ProfileState {
  const factory ProfileFetchFailed() = _$ProfileFetchFailed;
}

/// @nodoc
mixin _$ProfileEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchProfile,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FetchProfile value) fetchProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FetchProfile value)? fetchProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FetchProfile value)? fetchProfile,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEventCopyWith<$Res> {
  factory $ProfileEventCopyWith(
          ProfileEvent value, $Res Function(ProfileEvent) then) =
      _$ProfileEventCopyWithImpl<$Res, ProfileEvent>;
}

/// @nodoc
class _$ProfileEventCopyWithImpl<$Res, $Val extends ProfileEvent>
    implements $ProfileEventCopyWith<$Res> {
  _$ProfileEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_FetchProfileCopyWith<$Res> {
  factory _$$_FetchProfileCopyWith(
          _$_FetchProfile value, $Res Function(_$_FetchProfile) then) =
      __$$_FetchProfileCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_FetchProfileCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res, _$_FetchProfile>
    implements _$$_FetchProfileCopyWith<$Res> {
  __$$_FetchProfileCopyWithImpl(
      _$_FetchProfile _value, $Res Function(_$_FetchProfile) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_FetchProfile implements _FetchProfile {
  const _$_FetchProfile();

  @override
  String toString() {
    return 'ProfileEvent.fetchProfile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_FetchProfile);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchProfile,
  }) {
    return fetchProfile();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchProfile,
  }) {
    return fetchProfile?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchProfile,
    required TResult orElse(),
  }) {
    if (fetchProfile != null) {
      return fetchProfile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FetchProfile value) fetchProfile,
  }) {
    return fetchProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FetchProfile value)? fetchProfile,
  }) {
    return fetchProfile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FetchProfile value)? fetchProfile,
    required TResult orElse(),
  }) {
    if (fetchProfile != null) {
      return fetchProfile(this);
    }
    return orElse();
  }
}

abstract class _FetchProfile implements ProfileEvent {
  const factory _FetchProfile() = _$_FetchProfile;
}
