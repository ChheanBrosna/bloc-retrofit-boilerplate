part of 'profile_bloc.dart';

@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState.profileNotFetch() = ProfileNotFetch;
  const factory ProfileState.profileFetchInProgress() = ProfileFetchInProgress;
  const factory ProfileState.profileFetchSucceed() = ProfileFetchSucceed;
  const factory ProfileState.profileFetchFailed() = ProfileFetchFailed;
}
