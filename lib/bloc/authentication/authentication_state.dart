part of 'authentication_bloc.dart';

@freezed
class AuthenticationState with _$AuthenticationState {
  const factory AuthenticationState.unauthenticated() = Unauthenticated;
  const factory AuthenticationState.authenticating() = Authenticating;
  const factory AuthenticationState.authenticated() = Authenticated;
  const factory AuthenticationState.authenticateFailed() = AuthenticateFailed;
}
