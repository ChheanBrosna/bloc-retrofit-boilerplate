part of 'product_bloc.dart';

@freezed
class ProductState with _$ProductState {
  const factory ProductState.productNotFetch() = ProductNotFetch;
  const factory ProductState.productFetchInProgress() = ProductFetchInProgress;
  const factory ProductState.productFetchSucceed(
          {required ProductResponseModel productResponseModel}) =
      ProductFetchSucceed;
  const factory ProductState.productFetchFailed() = ProductFetchFailed;
}
