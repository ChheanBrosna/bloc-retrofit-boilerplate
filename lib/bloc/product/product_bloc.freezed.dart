// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProductState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() productNotFetch,
    required TResult Function() productFetchInProgress,
    required TResult Function(ProductResponseModel productResponseModel)
        productFetchSucceed,
    required TResult Function() productFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? productNotFetch,
    TResult? Function()? productFetchInProgress,
    TResult? Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult? Function()? productFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? productNotFetch,
    TResult Function()? productFetchInProgress,
    TResult Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult Function()? productFetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductNotFetch value) productNotFetch,
    required TResult Function(ProductFetchInProgress value)
        productFetchInProgress,
    required TResult Function(ProductFetchSucceed value) productFetchSucceed,
    required TResult Function(ProductFetchFailed value) productFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductNotFetch value)? productNotFetch,
    TResult? Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult? Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult? Function(ProductFetchFailed value)? productFetchFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductNotFetch value)? productNotFetch,
    TResult Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult Function(ProductFetchFailed value)? productFetchFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductStateCopyWith<$Res> {
  factory $ProductStateCopyWith(
          ProductState value, $Res Function(ProductState) then) =
      _$ProductStateCopyWithImpl<$Res, ProductState>;
}

/// @nodoc
class _$ProductStateCopyWithImpl<$Res, $Val extends ProductState>
    implements $ProductStateCopyWith<$Res> {
  _$ProductStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ProductNotFetchCopyWith<$Res> {
  factory _$$ProductNotFetchCopyWith(
          _$ProductNotFetch value, $Res Function(_$ProductNotFetch) then) =
      __$$ProductNotFetchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProductNotFetchCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductNotFetch>
    implements _$$ProductNotFetchCopyWith<$Res> {
  __$$ProductNotFetchCopyWithImpl(
      _$ProductNotFetch _value, $Res Function(_$ProductNotFetch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProductNotFetch implements ProductNotFetch {
  const _$ProductNotFetch();

  @override
  String toString() {
    return 'ProductState.productNotFetch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProductNotFetch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() productNotFetch,
    required TResult Function() productFetchInProgress,
    required TResult Function(ProductResponseModel productResponseModel)
        productFetchSucceed,
    required TResult Function() productFetchFailed,
  }) {
    return productNotFetch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? productNotFetch,
    TResult? Function()? productFetchInProgress,
    TResult? Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult? Function()? productFetchFailed,
  }) {
    return productNotFetch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? productNotFetch,
    TResult Function()? productFetchInProgress,
    TResult Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult Function()? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productNotFetch != null) {
      return productNotFetch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductNotFetch value) productNotFetch,
    required TResult Function(ProductFetchInProgress value)
        productFetchInProgress,
    required TResult Function(ProductFetchSucceed value) productFetchSucceed,
    required TResult Function(ProductFetchFailed value) productFetchFailed,
  }) {
    return productNotFetch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductNotFetch value)? productNotFetch,
    TResult? Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult? Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult? Function(ProductFetchFailed value)? productFetchFailed,
  }) {
    return productNotFetch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductNotFetch value)? productNotFetch,
    TResult Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult Function(ProductFetchFailed value)? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productNotFetch != null) {
      return productNotFetch(this);
    }
    return orElse();
  }
}

abstract class ProductNotFetch implements ProductState {
  const factory ProductNotFetch() = _$ProductNotFetch;
}

/// @nodoc
abstract class _$$ProductFetchInProgressCopyWith<$Res> {
  factory _$$ProductFetchInProgressCopyWith(_$ProductFetchInProgress value,
          $Res Function(_$ProductFetchInProgress) then) =
      __$$ProductFetchInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProductFetchInProgressCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductFetchInProgress>
    implements _$$ProductFetchInProgressCopyWith<$Res> {
  __$$ProductFetchInProgressCopyWithImpl(_$ProductFetchInProgress _value,
      $Res Function(_$ProductFetchInProgress) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProductFetchInProgress implements ProductFetchInProgress {
  const _$ProductFetchInProgress();

  @override
  String toString() {
    return 'ProductState.productFetchInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProductFetchInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() productNotFetch,
    required TResult Function() productFetchInProgress,
    required TResult Function(ProductResponseModel productResponseModel)
        productFetchSucceed,
    required TResult Function() productFetchFailed,
  }) {
    return productFetchInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? productNotFetch,
    TResult? Function()? productFetchInProgress,
    TResult? Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult? Function()? productFetchFailed,
  }) {
    return productFetchInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? productNotFetch,
    TResult Function()? productFetchInProgress,
    TResult Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult Function()? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchInProgress != null) {
      return productFetchInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductNotFetch value) productNotFetch,
    required TResult Function(ProductFetchInProgress value)
        productFetchInProgress,
    required TResult Function(ProductFetchSucceed value) productFetchSucceed,
    required TResult Function(ProductFetchFailed value) productFetchFailed,
  }) {
    return productFetchInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductNotFetch value)? productNotFetch,
    TResult? Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult? Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult? Function(ProductFetchFailed value)? productFetchFailed,
  }) {
    return productFetchInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductNotFetch value)? productNotFetch,
    TResult Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult Function(ProductFetchFailed value)? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchInProgress != null) {
      return productFetchInProgress(this);
    }
    return orElse();
  }
}

abstract class ProductFetchInProgress implements ProductState {
  const factory ProductFetchInProgress() = _$ProductFetchInProgress;
}

/// @nodoc
abstract class _$$ProductFetchSucceedCopyWith<$Res> {
  factory _$$ProductFetchSucceedCopyWith(_$ProductFetchSucceed value,
          $Res Function(_$ProductFetchSucceed) then) =
      __$$ProductFetchSucceedCopyWithImpl<$Res>;
  @useResult
  $Res call({ProductResponseModel productResponseModel});

  $ProductResponseModelCopyWith<$Res> get productResponseModel;
}

/// @nodoc
class __$$ProductFetchSucceedCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductFetchSucceed>
    implements _$$ProductFetchSucceedCopyWith<$Res> {
  __$$ProductFetchSucceedCopyWithImpl(
      _$ProductFetchSucceed _value, $Res Function(_$ProductFetchSucceed) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productResponseModel = null,
  }) {
    return _then(_$ProductFetchSucceed(
      productResponseModel: null == productResponseModel
          ? _value.productResponseModel
          : productResponseModel // ignore: cast_nullable_to_non_nullable
              as ProductResponseModel,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ProductResponseModelCopyWith<$Res> get productResponseModel {
    return $ProductResponseModelCopyWith<$Res>(_value.productResponseModel,
        (value) {
      return _then(_value.copyWith(productResponseModel: value));
    });
  }
}

/// @nodoc

class _$ProductFetchSucceed implements ProductFetchSucceed {
  const _$ProductFetchSucceed({required this.productResponseModel});

  @override
  final ProductResponseModel productResponseModel;

  @override
  String toString() {
    return 'ProductState.productFetchSucceed(productResponseModel: $productResponseModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductFetchSucceed &&
            (identical(other.productResponseModel, productResponseModel) ||
                other.productResponseModel == productResponseModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, productResponseModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductFetchSucceedCopyWith<_$ProductFetchSucceed> get copyWith =>
      __$$ProductFetchSucceedCopyWithImpl<_$ProductFetchSucceed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() productNotFetch,
    required TResult Function() productFetchInProgress,
    required TResult Function(ProductResponseModel productResponseModel)
        productFetchSucceed,
    required TResult Function() productFetchFailed,
  }) {
    return productFetchSucceed(productResponseModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? productNotFetch,
    TResult? Function()? productFetchInProgress,
    TResult? Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult? Function()? productFetchFailed,
  }) {
    return productFetchSucceed?.call(productResponseModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? productNotFetch,
    TResult Function()? productFetchInProgress,
    TResult Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult Function()? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchSucceed != null) {
      return productFetchSucceed(productResponseModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductNotFetch value) productNotFetch,
    required TResult Function(ProductFetchInProgress value)
        productFetchInProgress,
    required TResult Function(ProductFetchSucceed value) productFetchSucceed,
    required TResult Function(ProductFetchFailed value) productFetchFailed,
  }) {
    return productFetchSucceed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductNotFetch value)? productNotFetch,
    TResult? Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult? Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult? Function(ProductFetchFailed value)? productFetchFailed,
  }) {
    return productFetchSucceed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductNotFetch value)? productNotFetch,
    TResult Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult Function(ProductFetchFailed value)? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchSucceed != null) {
      return productFetchSucceed(this);
    }
    return orElse();
  }
}

abstract class ProductFetchSucceed implements ProductState {
  const factory ProductFetchSucceed(
          {required final ProductResponseModel productResponseModel}) =
      _$ProductFetchSucceed;

  ProductResponseModel get productResponseModel;
  @JsonKey(ignore: true)
  _$$ProductFetchSucceedCopyWith<_$ProductFetchSucceed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductFetchFailedCopyWith<$Res> {
  factory _$$ProductFetchFailedCopyWith(_$ProductFetchFailed value,
          $Res Function(_$ProductFetchFailed) then) =
      __$$ProductFetchFailedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProductFetchFailedCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductFetchFailed>
    implements _$$ProductFetchFailedCopyWith<$Res> {
  __$$ProductFetchFailedCopyWithImpl(
      _$ProductFetchFailed _value, $Res Function(_$ProductFetchFailed) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProductFetchFailed implements ProductFetchFailed {
  const _$ProductFetchFailed();

  @override
  String toString() {
    return 'ProductState.productFetchFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProductFetchFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() productNotFetch,
    required TResult Function() productFetchInProgress,
    required TResult Function(ProductResponseModel productResponseModel)
        productFetchSucceed,
    required TResult Function() productFetchFailed,
  }) {
    return productFetchFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? productNotFetch,
    TResult? Function()? productFetchInProgress,
    TResult? Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult? Function()? productFetchFailed,
  }) {
    return productFetchFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? productNotFetch,
    TResult Function()? productFetchInProgress,
    TResult Function(ProductResponseModel productResponseModel)?
        productFetchSucceed,
    TResult Function()? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchFailed != null) {
      return productFetchFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductNotFetch value) productNotFetch,
    required TResult Function(ProductFetchInProgress value)
        productFetchInProgress,
    required TResult Function(ProductFetchSucceed value) productFetchSucceed,
    required TResult Function(ProductFetchFailed value) productFetchFailed,
  }) {
    return productFetchFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductNotFetch value)? productNotFetch,
    TResult? Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult? Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult? Function(ProductFetchFailed value)? productFetchFailed,
  }) {
    return productFetchFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductNotFetch value)? productNotFetch,
    TResult Function(ProductFetchInProgress value)? productFetchInProgress,
    TResult Function(ProductFetchSucceed value)? productFetchSucceed,
    TResult Function(ProductFetchFailed value)? productFetchFailed,
    required TResult orElse(),
  }) {
    if (productFetchFailed != null) {
      return productFetchFailed(this);
    }
    return orElse();
  }
}

abstract class ProductFetchFailed implements ProductState {
  const factory ProductFetchFailed() = _$ProductFetchFailed;
}

/// @nodoc
mixin _$ProductEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchProduct,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FetchProduct value) fetchProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FetchProduct value)? fetchProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FetchProduct value)? fetchProduct,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductEventCopyWith<$Res> {
  factory $ProductEventCopyWith(
          ProductEvent value, $Res Function(ProductEvent) then) =
      _$ProductEventCopyWithImpl<$Res, ProductEvent>;
}

/// @nodoc
class _$ProductEventCopyWithImpl<$Res, $Val extends ProductEvent>
    implements $ProductEventCopyWith<$Res> {
  _$ProductEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_FetchProductCopyWith<$Res> {
  factory _$$_FetchProductCopyWith(
          _$_FetchProduct value, $Res Function(_$_FetchProduct) then) =
      __$$_FetchProductCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_FetchProductCopyWithImpl<$Res>
    extends _$ProductEventCopyWithImpl<$Res, _$_FetchProduct>
    implements _$$_FetchProductCopyWith<$Res> {
  __$$_FetchProductCopyWithImpl(
      _$_FetchProduct _value, $Res Function(_$_FetchProduct) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_FetchProduct implements _FetchProduct {
  const _$_FetchProduct();

  @override
  String toString() {
    return 'ProductEvent.fetchProduct()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_FetchProduct);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchProduct,
  }) {
    return fetchProduct();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchProduct,
  }) {
    return fetchProduct?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchProduct,
    required TResult orElse(),
  }) {
    if (fetchProduct != null) {
      return fetchProduct();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FetchProduct value) fetchProduct,
  }) {
    return fetchProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FetchProduct value)? fetchProduct,
  }) {
    return fetchProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FetchProduct value)? fetchProduct,
    required TResult orElse(),
  }) {
    if (fetchProduct != null) {
      return fetchProduct(this);
    }
    return orElse();
  }
}

abstract class _FetchProduct implements ProductEvent {
  const factory _FetchProduct() = _$_FetchProduct;
}
