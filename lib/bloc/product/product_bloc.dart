import 'package:bloc_retrofit_boilerplate/main.dart';
import 'package:bloc_retrofit_boilerplate/mixin/mixin_index.dart';
import 'package:bloc_retrofit_boilerplate/model/model_index.dart';
import 'package:bloc_retrofit_boilerplate/repository/repository_index.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_state.dart';
part 'product_event.dart';
part 'product_bloc.freezed.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> with DIOMixin {
  ProductBloc() : super(const ProductState.productNotFetch()) {
    on<_FetchProduct>(_onFetchProduct);
  }
  void _onFetchProduct(_FetchProduct event, Emitter<ProductState> emit) async {
    try {
      LoadingModal.show();
      emit(const ProductState.productFetchInProgress());

      final dio = await getDio();
      if (dio == null) throw Exception(const NoConnection().message);

      final repository = ProductRepository(dio);
      final response = await repository.getProduct();

      emit(ProductState.productFetchSucceed(productResponseModel: response));
      LoadingModal.hide();

      // Dio Error Catch
    } on DioError catch (error) {
      emit(const ProductState.productFetchFailed());
      LoadingModal.hide();
      globalUiErrorSubject.add(error.message);

      // UnExpected Error Catch
    } catch (error) {
      emit(const ProductState.productFetchFailed());
      LoadingModal.hide();
      globalUiErrorSubject.add(error.toString());
    }
  }
}
