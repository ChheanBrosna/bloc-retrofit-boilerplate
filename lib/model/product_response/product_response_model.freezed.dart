// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'product_response_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductResponseModel _$ProductResponseModelFromJson(Map<String, dynamic> json) {
  return _ProductResponseModel.fromJson(json);
}

/// @nodoc
mixin _$ProductResponseModel {
  List<ProductModel> get products => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductResponseModelCopyWith<ProductResponseModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductResponseModelCopyWith<$Res> {
  factory $ProductResponseModelCopyWith(ProductResponseModel value,
          $Res Function(ProductResponseModel) then) =
      _$ProductResponseModelCopyWithImpl<$Res, ProductResponseModel>;
  @useResult
  $Res call({List<ProductModel> products});
}

/// @nodoc
class _$ProductResponseModelCopyWithImpl<$Res,
        $Val extends ProductResponseModel>
    implements $ProductResponseModelCopyWith<$Res> {
  _$ProductResponseModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? products = null,
  }) {
    return _then(_value.copyWith(
      products: null == products
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<ProductModel>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProductResponseModelCopyWith<$Res>
    implements $ProductResponseModelCopyWith<$Res> {
  factory _$$_ProductResponseModelCopyWith(_$_ProductResponseModel value,
          $Res Function(_$_ProductResponseModel) then) =
      __$$_ProductResponseModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ProductModel> products});
}

/// @nodoc
class __$$_ProductResponseModelCopyWithImpl<$Res>
    extends _$ProductResponseModelCopyWithImpl<$Res, _$_ProductResponseModel>
    implements _$$_ProductResponseModelCopyWith<$Res> {
  __$$_ProductResponseModelCopyWithImpl(_$_ProductResponseModel _value,
      $Res Function(_$_ProductResponseModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? products = null,
  }) {
    return _then(_$_ProductResponseModel(
      products: null == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<ProductModel>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProductResponseModel implements _ProductResponseModel {
  _$_ProductResponseModel({required final List<ProductModel> products})
      : _products = products;

  factory _$_ProductResponseModel.fromJson(Map<String, dynamic> json) =>
      _$$_ProductResponseModelFromJson(json);

  final List<ProductModel> _products;
  @override
  List<ProductModel> get products {
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  String toString() {
    return 'ProductResponseModel(products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductResponseModel &&
            const DeepCollectionEquality().equals(other._products, _products));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_products));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProductResponseModelCopyWith<_$_ProductResponseModel> get copyWith =>
      __$$_ProductResponseModelCopyWithImpl<_$_ProductResponseModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductResponseModelToJson(
      this,
    );
  }
}

abstract class _ProductResponseModel implements ProductResponseModel {
  factory _ProductResponseModel({required final List<ProductModel> products}) =
      _$_ProductResponseModel;

  factory _ProductResponseModel.fromJson(Map<String, dynamic> json) =
      _$_ProductResponseModel.fromJson;

  @override
  List<ProductModel> get products;
  @override
  @JsonKey(ignore: true)
  _$$_ProductResponseModelCopyWith<_$_ProductResponseModel> get copyWith =>
      throw _privateConstructorUsedError;
}
