import 'package:bloc_retrofit_boilerplate/model/model_index.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_response_model.freezed.dart';
part 'product_response_model.g.dart';

@freezed
class ProductResponseModel with _$ProductResponseModel {
  factory ProductResponseModel({
    required List<ProductModel> products,
  }) = _ProductResponseModel;

  factory ProductResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ProductResponseModelFromJson(json);
}
