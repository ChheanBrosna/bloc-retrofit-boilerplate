// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProductResponseModel _$$_ProductResponseModelFromJson(
        Map<String, dynamic> json) =>
    _$_ProductResponseModel(
      products: (json['products'] as List<dynamic>)
          .map((e) => ProductModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ProductResponseModelToJson(
        _$_ProductResponseModel instance) =>
    <String, dynamic>{
      'products': instance.products,
    };
